"""Classes for bots and human player class to use in Game"""
import numpy as np
import numba as nb
from abc import ABC, abstractmethod

from .board import Board
from .board import Game
import logging
logging.basicConfig(level=logging.WARN)


# Base player class
class Player(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def next_move(self, myboard:Board):
        pass


# helpers for MM recursion

@nb.jit(nopython=True)
def fast_legalmoves(board:nb.uint8[:,:]):
    legalmoves = []
    for col in range(board.shape[1]):
        if fast_lowest_open_row(board, col) >= 0:
            legalmoves.append(col)
    return legalmoves

@nb.jit(nopython=True)
def fast_make_move(board:nb.uint8[:,:], col:nb.uint8, player:nb.uint8):
    row = fast_lowest_open_row(board, col)
    board[row][col] = player
    return board

@nb.jit(nopython=True)
def fast_delete_move(board:nb.uint8[:,:], col:nb.uint8):
    row = fast_lowest_open_row(board, col)
    board[row + 1][col] = 0
    return board

@nb.jit(nopython=True)
def fast_lowest_open_row(board:nb.uint8[:,:], col:nb.uint8):
    for i in range(board.shape[0]-1, -1, -1):
        if board[i][col] == 0:
            return i
    return -1

@nb.jit(nopython=True)
def board_score(player, board, winner, depth, winscore, win_length):
    '''Scoring function for minimax. Wins discounted by depth and otherwise counts near_wins.'''
    opp_player = (player % 2) + 1
    if winner == player:
        return winscore - depth
    elif winner == opp_player:
        return -winscore + depth
    else:
        playernearwin = count_near_wins(board, player, win_length, win_length-1)
        enemynearwin = count_near_wins(board, opp_player, win_length, win_length-1)
        return 2*(playernearwin - enemynearwin)

@nb.jit(nopython=True)
def count_near_wins(board, piecenum, win_length, count):
    """Uses _check_slice_loop to find number of places where there are count pieces in a win_length consecutive group"""
    total = 0
    # horizontals
    for startcol in range(board.shape[1] - win_length + 1):
        for startrow in range(board.shape[0]):
            total += _check_slice_loop(board, piecenum, startrow, startcol, 
                                            0, 1, win_length, count)
            
    # verticals
    for startcol in range(board.shape[1]):
        for startrow in range(board.shape[0] - win_length + 1):
            total += _check_slice_loop(board, piecenum, startrow, startcol, 
                                            1, 0, win_length, count)
            
    # diagonal down-right
    for startcol in range(board.shape[1] - win_length + 1):
        for startrow in range(board.shape[0] - win_length + 1):
            total += _check_slice_loop(board, piecenum, startrow, startcol,
                                            1, 1, win_length, count)
    
    # diagonal down-left
    for startcol in range(win_length - 1, board.shape[1]):
        for startrow in range(board.shape[0] - win_length + 1):
            total += _check_slice_loop(board, piecenum, startrow, startcol,
                                            1, -1, win_length, count)
    return total

@nb.jit(nopython=True)
def _check_slice_loop(board, piece, startrow, startcol, 
                      rowincr, colincr, win_length, count):
    """Looks in direction of (rowincr, colincr) from (startrow, startcol) and counts matches to piece.
        Returns 1 if and only if at least count matches among win_length pieces with no enemy pieces; 0 else."""
    if board[startrow, startcol] == piece:
        total = 1
    else:
        total = 0
    for j in range(1,win_length):
        nextrow = (startrow + j * rowincr)
        nextcol = (startcol + j * colincr)
        nextpiece = board[nextrow, nextcol]
        if piece == nextpiece:
            total += 1
        elif piece != 0:
            return int(total >= count)
    return int(total >= count)

@nb.jit(nopython=True)
def _count_slice_loop(board, piece, startrow, startcol, 
                      rowincr, colincr, max_length):
    '''Looks in direction of incr up to max_length-1 steps from start point, counts consecutive pieces matching piece.'''
    total = 0
    height = board.shape[0]
    width = board.shape[1]
    for j in range(1, max_length):
        nextrow = (startrow + j * rowincr)
        nextcol = (startcol + j * colincr)
        valid_row = (nextrow < height) & (nextrow >= 0)
        valid_col = (nextcol < width) & (nextcol >= 0)
        if not (valid_row & valid_col):
            return total
        nextpiece = board[nextrow, nextcol]
        if piece == nextpiece:
            total += 1
        else:
            return total
    return total


@nb.jit(nopython=True)
def fast_check_winner(board:nb.uint8[:, :], win_length:nb.uint8, 
                      move:nb.uint8):
    '''Reimplementation of board win checker as JIT function.'''
    row = fast_lowest_open_row(board, move) + 1
    col = move
    piece = board[row, col]
    
    # horizontal-left
    hl = _count_slice_loop(board, piece, row, col, 0, -1, win_length)
    # horizontal-right
    hr = _count_slice_loop(board, piece, row, col, 0, 1, win_length)
    if hl + hr + 1 >= win_length:
        return piece

    # vertical-down (skip vertical up)
    vd = _count_slice_loop(board, piece, row, col, 1, 0, win_length)
    if vd + 1 >= win_length:
        return piece

    # diagonal down-right
    dr = _count_slice_loop(board, piece, row, col, 1, 1, win_length)
    # diagonal up-left
    ul = _count_slice_loop(board, piece, row, col, -1, -1, win_length)
    if dr + ul + 1 >= win_length:
        return piece

    # diagonal down-left
    dl = _count_slice_loop(board, piece, row, col, 1, -1, win_length)
    # diagonal up-right
    ur = _count_slice_loop(board, piece, row, col, -1, 1, win_length)
    if dl + ur + 1 >= win_length:
        return piece

    # no winner
    return 0


@nb.jit(nopython=True)
def fast_minimax(board:nb.uint8[:, :], depth:nb.uint8, maximize:nb.boolean, 
                 thisplayer:nb.uint8, win_length:nb.uint8, max_depth:nb.uint8,
                 winscore:nb.uint8):
    '''Minimax method in JIT style for maximizing score using board_score function.'''
    legalmoves = fast_legalmoves(board)
    if maximize:
        move_player = thisplayer
    else:
        move_player = (thisplayer % 2) + 1
    scores = []
    for move in legalmoves:
        board = fast_make_move(board, move, move_player)
        movecheck = fast_legalmoves(board)
        winner = fast_check_winner(board, win_length, move)
        if (winner != 0) | (depth == max_depth) | (len(movecheck) == 0):
            next_score = board_score(thisplayer, board, winner, depth, winscore, win_length)
        else:
            _, _, next_score = fast_minimax(board, depth + 1, not maximize, thisplayer, 
                                         win_length, max_depth, winscore)
        board = fast_delete_move(board, move)
        scores.append(next_score)
    if depth == 0:
        return legalmoves, scores, 0
    else:
        if maximize:
            bestscore = -100
            for score in scores:
                if score > bestscore:
                    bestscore = score
            return [0], [0], bestscore
        else:
            worstscore = 100
            for score in scores:
                if score < worstscore:
                    worstscore = score
            return [0], [0], worstscore
        
# Player classes

class MMBot(Player):
    """Implementation of a recursive minimax move selector with fixed depth."""
    def __init__(self, playernum, max_depth=7, win_length=4):
        print("Minimax bot implementation attempt")
        self.playernum = playernum
        self.opp_playernum = (self.playernum % 2) + 1
        self.max_depth = max_depth
        self.winscore = 50
        self.win_length = win_length
    
    def choose_move(self, moves, scores):
        goodmoves = np.argwhere(scores == np.max(scores)).T[0]
        moveind = np.random.choice(goodmoves)
        logging.info(f'Moves: {moves}, Scores: {scores}')
        return moves[moveind]
    
    def next_move(self, myboard:Board):
        moves, scores, _ = fast_minimax(myboard.state, 0, True, 
                 self.playernum, self.win_length, self.max_depth,
                 self.winscore)
        return self.choose_move(moves, scores)




class EasyBot(Player):
    """Looks one move ahead and tries to win if available or block opponent's win. Random move otherwise."""
    def __init__(self, playernum):
        print("Knows the basic rules but doesn't strategize.")
        self.playernum = playernum
        self.opp_playernum = (self.playernum % 2) + 1

    def next_move(self, myboard:Board):
        legalmoves = myboard.legal_moves()
        goodmoves = legalmoves.copy()
        losingmoves = []
        blockingmoves = []
        winningmoves = []
        for move in legalmoves:
            myboard.next_move(move)
            if myboard.check_last_move_lines(move) == self.playernum:
                winningmoves.append(move)
            for opp_player_move in myboard.legal_moves():
                myboard.next_move(opp_player_move)
                if myboard.check_last_move_lines(opp_player_move) == self.opp_playernum:
                    losingmoves.append(move)
                    if opp_player_move != move:
                        blockingmoves.append(opp_player_move)
                    else:
                        goodmoves.remove(move)
                myboard.delete_move(opp_player_move)
            myboard.delete_move(move)
        logging.info(f"Legal moves: {legalmoves}")
        if len(losingmoves) > 0:
            logging.info(f"Losing moves: {list(set(losingmoves))}")
        if len(winningmoves) > 0:
            logging.info(f"Winning moves: {list(set(winningmoves))}")
            randommove = np.random.choice(list(set(winningmoves)))
            movetype = "Winning move"
        elif len(blockingmoves) > 0:
            logging.info(f"Blocking moves: {list(set(blockingmoves))}")
            randommove = np.random.choice(list(set(blockingmoves)))
            movetype = "Blocking move"
        elif len(goodmoves) > 0:
            randommove = np.random.choice(goodmoves)
            movetype = "Random move"
        else:
            randommove = np.random.choice(legalmoves)
            movetype = "No good moves left! Going at" 
        logging.info(f"{movetype} {randommove}")
        return randommove

    def random_move(self, board):
        legal_moves = board.legal_moves()
        if len(legal_moves) > 0:
            return np.random.choice(board.legal_moves())
        else:
            logging.info("No legal moves left!")
            return "reset"




class HumanPlayer(Player):
    """Each turn, prompts user to enter next move."""
    def __init__(self, _):
        print("Enter column number each turn to move.")
        self.displaydict = {0: ' ', 1: 'X', 2: 'O'}
    def next_move(self, myboard):
        novalidmove = True
        while novalidmove:
            enternextmove = input(f"Next move for {self.displaydict[myboard.active_player]}: ")
            try:
                nextmove = int(enternextmove)
                assert nextmove in myboard.legal_moves()
                novalidmove = False
            except:
                print('Invalid move')
        return nextmove

class RandomPlayer(Player):
    """Each turn picks a random legal move."""
    def __init__(self, _):
        print("This bot will play random moves.")
    def next_move(self, board):
        legal_moves = board.legal_moves()
        if len(legal_moves) > 0:
            return np.random.choice(board.legal_moves())
        else:
            print("No legal moves left!")
            return board.error_val


