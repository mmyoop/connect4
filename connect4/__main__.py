"""Run the Connect Four game in a terminal window"""

from . import board
from . import players

playertypedict = {'human':players.HumanPlayer, 'randombot':players.RandomPlayer,
                  'easybot':players.EasyBot, 'mediumbot': (lambda x: players.MMBot(x, max_depth = 3)),
                  'hardbot':players.MMBot}



def newgame():
    print("Welcome to the game")
    players = []
    for j in range(2):
        novalidinput = True
        while novalidinput:
            playertypename = input(
                f"Who will be player {j+1}? Choose from {list(playertypedict.keys())}. "
            )
            if playertypename in playertypedict.keys():
                novalidinput = False
                players.append(playertypedict[playertypename](j+1))
            else:
                print(f"{playertypename} is not a valid player type. Please correct your entry.")
    thisgame = board.Game(players[0], players[1])
    return thisgame

if __name__ == "__main__":
    thisgame = newgame()
    thisgame.run_game()
    exit()
