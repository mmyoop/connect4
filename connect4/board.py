"""Major classes for basic text-based game for 2 players"""

import numba as nb
import numpy as np
import copy
import traceback
import time

from .gui import BoardGUI



boardspec = [
    ('state', nb.uint8[:, :]),
    ('active_player', nb.uint8),
    ('inactive_player',nb.uint8),
    ('active_game', nb.boolean),
    ('win_length', nb.uint8),
    ('error_val', nb.uint8)
]
@nb.experimental.jitclass(boardspec)
class Board:
    """Simple board class to hold current state of connect 4 game"""
    
    def __init__(self):
        self.state = np.zeros((6,7), dtype = np.uint8)
        self.active_player = 1
        self.inactive_player = 2
        self.active_game = True
        self.win_length = 4
        self.error_val = 255

    def swap_active_player(self):
        """Switches active player."""
        tmp = self.active_player
        self.active_player = self.inactive_player
        self.inactive_player = tmp
        


    def legal_moves(self):
        legalmoves = []
        for col in range(self.state.shape[1]):
            if self._lowest_open_row(col) >= 0:
                legalmoves.append(col)
        return legalmoves

    def _lowest_open_row(self, col):
        if col not in range(self.state.shape[1]):
            raise ValueError
        else:
            row = -1
            for i in range(self.state.shape[0]):
                if self.state[i][col] == 0:
                    row = i
        return row


    def next_move(self, col):
        """Drops piece into lowest open row in given col. Returns Boolean for valid move."""
        try:
            col = int(col)
            assert col in self.legal_moves()
        except:
            return (self.error_val, self.error_val)
        row = self._lowest_open_row(col)
        self.state[row][col] = self.active_player
        self.swap_active_player()
        return (row, col)

    def delete_move(self, col):
        """Reverses last move in column col. Used by bots checking moves."""
        row = self._lowest_open_row(col)
        if  row == self.state.shape[0] - 1:
            raise ValueError
        else:
            self.state[row + 1][col] = 0
            self.swap_active_player()
            return (row + 1, col)

    def check_winner(self, col):
        winner = self.check_last_move_lines(col)
        if winner != 0:
            self.active_game = False
        return winner

    def check_last_move_lines(self, col, length = None):
        """Uses _check_slice_loop to find number of pieces matching last move in each direction"""
        if length is None:
            length = self.win_length        
        row = self._lowest_open_row(col) + 1
        piece = self.state[row][col]
        # horizontal-left
        hl = self._check_slice_loop(piece, row, col, 0, -1)
        # horizontal-right
        hr = self._check_slice_loop(piece, row, col, 0, 1)
        if hl + hr + 1 >= length:
            return piece

        # vertical-down (skip vertical up)
        vd = self._check_slice_loop(piece, row, col, 1, 0)
        if vd + 1 >= length:
            return piece

        # diagonal down-right
        dr = self._check_slice_loop(piece, row, col, 1, 1)
        # diagonal up-left
        ul = self._check_slice_loop(piece, row, col, -1, -1)
        if dr + ul + 1 >= length:
            return piece

        # diagonal down-left
        dl = self._check_slice_loop(piece, row, col, 1, -1)
        # diagonal up-right
        ur = self._check_slice_loop(piece, row, col, -1, 1)
        if dl + ur + 1 >= length:
            return piece

        # no winner
        return 0

    def _check_slice_loop(self, piece, startrow, startcol, rowincr, colincr):
        """Looks in direction of (rowincr, colincr) from (startrow, startcol) and counts matches to piece.
           Stops if anything doesn't match."""
        total = 0
        for j in range(1,self.win_length):
            nextrow = (startrow + j * rowincr)
            nextcol = (startcol + j * colincr)
            valid_row = (nextrow < self.state.shape[0]) & (nextrow >= 0)
            valid_col = (nextcol < self.state.shape[1]) & (nextcol >= 0)
            if not (valid_row & valid_col):
                break
            nextpiece = self.state[nextrow, nextcol]
            if piece == nextpiece:
                total += 1
            else:
                return total
        return total

class Game:
    """Sets up game with players."""
    def __init__(self, player1, player2) -> None:
        self.board = Board()
        self.displaydict = {0: ' ', 1: 'X', 2: 'O'}
        self.playerdict = {1: player1, 2: player2}
        self.numbaplayers = []

    def run_game(self):
        """Proceeds turn by turn with players until someone wins or a draw is reached."""
        gui = BoardGUI(np.zeros((6,7)))
        gui.draw_board()
        while self.board.active_game:
            nextplayer = self.playerdict[self.board.active_player]
            try:
                print(f"\n{self.displaydict[self.board.active_player]} Player's turn")
                print(self.print_board())
                playerboard = self.deep_copy_board()
                nextmove = nextplayer.next_move(playerboard)
                row, col = self.board.next_move(nextmove)
                print(f"{self.displaydict[self.board.inactive_player]} moved in column {nextmove}")
                if self.board.check_winner(nextmove) != 0:
                    print(f"Winner: {self.displaydict[self.board.inactive_player]}")
            except Exception as e:
                print(e)
                traceback.print_exc()
                print("Invalid entry")
                self.board.active_game = False
            gui.place_piece(row, col, self.board.inactive_player)
            if (len(self.board.legal_moves()) == 0) & self.board.active_game:
                print("No more legal moves! Game ends in a draw.")
                self.board.active_game = False
        time.sleep(5)
        print(self.print_board())
        print("Thank you for playing. Ending program.")

    def _row_divider(self):
        return self.board.state.shape[1] * "+---" + "+\n"

    def print_board(self):
        """Generates a string representation of the board"""
        boardstring = "\n"
        for j in range(self.board.state.shape[1]):
            boardstring += f"  {j} "
        boardstring += "\n"
        boardstring += self._row_divider()
        for row in range(self.board.state.shape[0]):
            for col in range(self.board.state.shape[1]):
                boardstring += f"| {self.displaydict[self.board.state[row][col]]} "
            boardstring += "|\n"
            boardstring += self._row_divider()
        return boardstring 
    
    def deep_copy_board(self):
        copyboard = Board()
        copyboard.active_player = self.board.active_player
        copyboard.inactive_player = self.board.inactive_player
        copyboard.active_game = self.board.active_game
        copyboard.state = copy.deepcopy(self.board.state)
        return copyboard

  
    # 
    # gui.draw_board()
    # gui.place_piece(2,3,1)
    # gui.place_piece(3,2,2)
    # gui.canvas.pack()
    # gui.mainloop()
