"""Board GUI for interactive game"""

import tkinter as tk
import numpy as np

class BoardGUI(tk.Tk):
    def __init__(self, board:np.array):
        self.board = board
        self.colors = {0 : 'white', 1 : 'red', 2 : 'yellow'}
        tk.Tk.__init__(self)
        self.shape = board.shape
        self.piecewidth = 100
        self.gap = int(self.piecewidth/10)
        self.diameter = 80
        self.xdim = board.shape[1] * self.piecewidth
        self.ydim = board.shape[0] * self.piecewidth
        self.geometry(f"{self.xdim}x{self.ydim}")
        self.canvas = tk.Canvas(self, width=self.xdim, height=self.ydim)

    def circle_location(self, row, col):
        return self.piecewidth * col + self.gap, self.piecewidth * row + self.gap
    
    def place_piece(self, row, col, piecenum):
        thisfill = self.colors[piecenum]
        loc_i, loc_j = self.circle_location(row, col)
        self.canvas.create_oval(loc_i, loc_j,
                                loc_i + self.diameter, 
                                loc_j + self.diameter, 
                                outline='black',
                                fill = thisfill)
        self.canvas.pack()
        self.canvas.update()


    def draw_board(self):
        self.canvas.create_rectangle(0, 0, 
                                     self.xdim, 
                                     self.ydim,
                                      outline='black', 
                                      fill='blue')
        for i in range(self.shape[1]):
            loc_i = self.piecewidth * i + self.gap
            for j in range(self.shape[0]):
                loc_j = self.piecewidth * j + self.gap
                thisfill = self.colors[self.board[j, i]]
                self.canvas.create_oval(loc_i, loc_j,
                                        loc_i + self.diameter, 
                                        loc_j + self.diameter, 
                                        outline='black',
                                        fill = thisfill)
        self.canvas.pack()
        self.canvas.update()

