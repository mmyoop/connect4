# connect4

## Name
Connect4

## Description
Standard 2-player Connect 4 game on a 6 x 7 grid. Pieces drop from the top and land on the lowest open row, with the goal being to get 4 in a row vertically, horizontally, or diagonally. Game can be played with 2 humans, 1 human and 1 bot, or 2 bots. There are 3 different bots: RandomBot which just plays random moves; EasyBot which knows to finish a 4-in-a-row or block an opponent's 3-in-a-row, but does random moves otherwise; and MMBot, which is an implementation of a minimax bot using Numba. The main goal is programming practice, touching on some Python features beyond the very basic loops and functions, as well as practice with Gitlab and VS Code. 

## Installation
Currently the only way to play this is by cloning the repo and running the code with `python -m connect4` from a Terminal window when in the right parent folder.
The code environment uses:
- Python 3.11.5
- Numba 0.58.0
- Numpy 1.25.2

but it will probably work with a fairly wide range of versions. 

## Usage
The program at the moment runs entirely in the terminal in a text-based manner. Upon startup, the user is prompted to choose between human and bot players for player 1 (X token) and 2 (O token). The game proceeds, with the bot(s) playing automatically and the human player(s) being prompted for text input, using the column number to decide where to "drop" their next piece. The game automatically terminates when there is a winner or when there are no legal moves remaining. 


## Roadmap
Here is a list of goals for this project, roughly in the order I plan to work on them:
- Make a GUI which runs in a new window and lets the user start by clicking for their player choices, and then the game runs with the user clicking for their column choice. I would like to draw it on the screen with colors for the pieces, and maybe also we can watch the pieces "drop" from the top to the bottom 
- Set it up to run on a server online using something like FastAPI 